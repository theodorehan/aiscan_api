# 1차 프로토콜 검토 

Protocol Sheet						
						
1	http://localhost:3100/listen/begin					
	Name	사진 쓰기 시작				
	Description	사진파일을 쓰기 시작하였을 때 요청 합니다				
	Resource Path	/listen/begin				
	Host	localhost:3100				
	Method	POST				
	Type	API				
	protocol	HTTP				
	Parameters ( x-www-form-urlencoded )					
	Name	Type	value	description		
	apiKey	string	"3be91a7e-8c13-4bfc-ba5c-b30f57e068ae"	그대로 전달 해주시면 됩니다		
	carId	string	"나1234123"	자동차 번호		
	charger	string	"010-0000-0000"	자동차 담당자 휴대폰 번호		
	status	string	"rent" | "return"	렌트 또는 반납 여부		
	directoryPath	string	"C:\path\to\base\{carId}\rent" | "C:\path\to\base\{carId}\return"	사진파일들을 쓸 디렉토리 위치		
	gps	string	"123.123412,1234.1234124"	위도,경도 ( 가능하시다면 전달 해주시면 좋을것 같습니다 )		
	Response					
	message	description				
	"fail"	실패				
	"wating"	성공				
						
2	http://localhost:3100/listen/finish					
	Name	사진 쓰기 완료				
	Description	사진파일을 모두 생성한후 요청 합니다				
	Resource Path	/listen/finish				
	Host	localhost:3100				
	Method	POST				
	Type	API				
	protocol	HTTP				
	Parameters ( x-www-form-urlencoded )					
	Name	Type	value	description		
	apiKey	string	"3be91a7e-8c13-4bfc-ba5c-b30f57e068ae"	그대로 전달 해주시면 됩니다		
	carId	string	"나1234123"	자동차 번호		
	Response					
	message	description				
	"fail"	실패				
	"reading"	성공				
						
3	http://WindowsServerIP:3000/car/{carId}/report					
	Name	보고서 페이지				
	Description	미디어 서버로 사진을 모두 전송 후 웹뷰로 이 페이지를 호출 하시면 됩니다.				
	Resource Path	/car/{carId}/report				
	Host	windows server ip:3000				
	Method	GET				
	Type	Web Page				
	protocol	HTTP				
	Parameters ( URL param )					
	Name	Type	value	description		
	carId	string	"나1234123"	자동차 번호		
	
## [프로토콜 검토 의견]

1. 로그인(차량번호 / 전화번호) 프로토콜이 없습니다.
2. 1번 사진 쓰기 시작 프로토콜과 2번 사진 쓰기 완료 프로토콜을 구분 없이 사진 쓰기 완료 정보만 전달해도 되지 않을까 생각합니다.
3. 위 설명처럼 사진 쓰기 완료만 전달 해도 되면 아래 4,5,6번 설명을 참고하여 1번 프로토콜과 2번 프로토콜을 통합하면 될 것 같습니다.
4. 1번 항목에서 렌트 또는 반납 여부 구분이 필요한지는 명태표한테 듣지 못해 확인해 보겠습니다.
5. 1번 항목에서 사진파일 디렉토리는 정보는 고정하여 사전 공유하면 되기때문에 프로토콜에 반영이 필요 없어 보입니다.
6. 1번 항목에서 gps 정보는 매 사진마다 포함되어 있기때문에 미디어 서버로 전송된 사진에 포함된 gps 정보를 활용해야 할 것 같습니다.


## 1차 프로토콜 검토 의견 답변
1. 로그인은 어디서 하게 되는건가요? 미디어서버? 캡쳐 앱? 또는 보고서 웹?
만약 미디어 서버에서 하게 된다면 2번 프로토콜에 통합 하면 될것 같습니다
보고서 웹에서 하게 된다면 제가 처리하면 될것 같습니다. - api 만
2. 1번 프로토콜은 2번에 통합 하도록 하겠습니다. ( 미리 준비하려고 하였습니다 )
4. 렌트 전과 후 한쌍의 데이터로 관리 하기 위해 필요합니다. ( 보고서에서 전/후가 쌍으로 제공 되어야 합니다 )
5. 명확한 디렉토리위치를 파악하기 위해 필요합니다. ( 사전 약속으로 대체 )
6. 영상으로 부터 캡쳐하여 사진을 제작하는것 아닌가요? 그럴 경우에도 사진에 GPS정보가 포함되는지 궁금합니다.