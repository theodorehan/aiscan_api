var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/login', async function(req, res, next) {
  const {
    carId,
    password,
  } = req.body;

  const conn = await req.getConn();

  console.log(carId, password);
  let carItem;
  try{
    const carResult = await conn.query(
        `SELECT car.seq as seq, carNo FROM car LEFT JOIN (carPw) ON car.seq=carPw.carSeq WHERE car.carNo=(?) AND carPw.password=(?) `
        ,[carId,password]
    );

    console.log(carResult);


    if( !carResult[0] ){
      conn.release();
      return res.status(404).send("fail");
    }

    carItem = carResult[0];
  } catch (e) {
    console.error(e);
  }

  conn.release();
  

  res.send('success');
});




module.exports = router;
