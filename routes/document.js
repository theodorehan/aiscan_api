
var express = require('express');
var cors = require('cors');
var router = express.Router();


router.get(`/:docId/scanResult`, cors(), async (req, res) => {
    const conn = await req.getConn();

    conn.query(
        `
SELECT 
 *
FROM scanResult  
WHERE rentDocumentSeq=(?)
ORDER BY fileIndex
;`
        , [req.params.docId]
    )
        .then((carResult) => {
            if( !carResult[0] ){
                return res.status(404).json({
                    message:'not found'
                })
            }


            res.json(carResult);
        })
        .catch((e) => {
            console.error(e);
            res.status(500).json({ message: e.toString() });
        })
        .finally(() => {
            conn.release();

        })

})


const COUNT_PER_PAGE = 10;
router.get(`/list`, cors(), async (req, res) => {
    const conn = await req.getConn();
    console.log('document list Queries', req.query);
    let {
        carSeq,
        page = 0,
    } = req.query;

    carSeq = parseInt(carSeq);
    page  = parseInt(page);

    conn.query(
        `
SELECT
 *
FROM rentDocument doc 
WHERE doc.carSeq=(?) 
ORDER BY doc.seq DESC
LIMIT ${page * COUNT_PER_PAGE},${COUNT_PER_PAGE}
;`
        , [carSeq]
    )
        .then(async (docList) => {
            if( !docList[0] ){
                return res.status(404).json({
                    message:'not found'
                })
            }

            const docSeqList = docList.map((d) => d.seq);

            const rets = await conn.query(
                `
SELECT
 *
FROM scanResult 
WHERE rentDocumentSeq IN (${docSeqList})
;`
                ,
            )

            console.log('rets',rets,docSeqList, rets.length);

            const docListWithResults = docList.map((doc) => ({...doc, scanResults: []}))
            const docReferMapBySeq = {};
            let doc;
            for(let i = 0; i < docListWithResults.length; i++ ){
                doc = docListWithResults[i];
                docReferMapBySeq[doc.seq] = doc;
            }

            for(let i = 0; i < rets.length; i++){
                let docSeq = rets[i].rentDocumentSeq;
                docReferMapBySeq[docSeq].scanResults.push(rets[i]);
            }

            res.json(docListWithResults);
        })
        .catch((e) => {
            console.error(e);
            res.status(500).json({ message: e.toString() });
        })
        .finally(() => {

            conn.release();

        })

})



module.exports = router;
