var express = require('express');
var multer = require('multer');
var fs = require('fs-extra');
var path = require('path');
var router = express.Router();
var upload = multer( )
var config = require('../config');

var uploadFields = [
  {name : 'file', maxCount:1 },
]
router.post('/upload', upload.single('file'), async function(req, res, next) {
  /**
   *
   * Request Params
   * - apiKey<String>: (UUID)
   * - carId<String>: 1234
   * - password<String>: 0000
   * - status<String>: "rent" | "return"
   * - file
   *
   * Response Text : "success" | "fail"
   */
  const {
    apiKey,
    carId,
    password,
    status,
    fileName,
  } = req.body;


  if( apiKey === '' ){

  }


  if( status !== 'rent' && status !== 'return' ){
    return res.status(400).send(`status[${status}] is not supported`);
  }


  console.log(carId, password, status);

  const conn = await req.getConn();

  let carItem;
  try{
    const carResult = await conn.query(
        `SELECT car.seq as seq, carNo FROM car LEFT JOIN (carPw) ON car.seq=carPw.carSeq WHERE car.carNo=(?) AND carPw.password=(?) `
        ,[carId,password]
    );

    console.log(carResult[0]);


    if( !carResult[0] ){
      conn.release();
      return res.status(404).send("not found car");
    }

    carItem = carResult[0];
  } catch (e) {
    console.error(e);
  }


    /**
     * Write new rentDocument
     */
  const result = await conn.query(
      "INSERT INTO rentDocument(carSeq, action, datetime, resourceGroupUUID, fileName ) " +
      `VALUES (${carItem.seq}, "${status}", "${req.getUTCTimestamp()}", "${'0000'}", "${ ( fileName || '' ).replace(/\:/g,'') }")`
  );

  const newSeq = result.insertId;

  console.log('new rentDocument',result);

    /**
     * Push new scan task into scanQueue
     */

  const resultInsertQueue = await conn.query(
      "INSERT INTO scanQueue(rentDocumentSeq, timestamp, working, progressStatus ) " +
      `VALUES ("${newSeq}", "${req.getUTCTimestamp()}", false, 0) `
  );
  console.log('new queue item',resultInsertQueue);


  conn.release();

  if( req.file ){

    const bundleFile = req.file.buffer;

    fs.writeFile(path.join(config.worker.bundleDir, 'rentdoc-'+ newSeq + '.mva'), bundleFile, (err) => {
      if (err) {
        console.error(err);
        res.status(500).write('fail')
      } else {
        console.log('The file has been saved!');

        res.send('success');

      }
    });
  } else {

    res.send('success');
  }

});


module.exports = router;
