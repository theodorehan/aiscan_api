var express = require('express');
var cors = require('cors');
var router = express.Router();

/* GET home page. */
router.get('/:carId/read', cors(), async function(req, res, next) {


    const conn = await req.getConn();


    conn.query(
            `SELECT car.seq as seq, carNo FROM car WHERE car.carNo=(?) `
        , [req.params.carId]
    )
        .then((carResult) => {
            if( !carResult[0] ){
                return res.status(404).json({
                    message:'not found'
                })
            }


            res.json(carResult[0]);
        })
        .catch((e) => {
            console.error(e);
            res.status(500).json({ message: e.toString() });
        })
        .finally(() => {
            conn.release();

        })


});



/* GET home page. */
router.get('/list', cors(), async function(req, res, next) {


    const conn = await req.getConn();


    conn.query(
        `SELECT car.seq as seq, carNo FROM car`
    )
        .then((carResult) => {
            if( !carResult[0] ){
                return res.status(404).json({
                    message:'not found'
                })
            }


            res.json(carResult);
        })
        .catch((e) => {
            console.error(e);
            res.status(500).json({ message: e.toString() });
        })
        .finally(() => {
            conn.release();

        })


});

router.get(`/:carSeq/lastTwoDocuments`, cors(), async (req, res) => {
    const conn = await req.getConn();

    conn.query(
        `
SELECT 
rentDocument.carSeq, 
rentDocument.seq, 
rentDocument.action, 
rentDocument.datetime, 
resourceGroupUUID,
lat,
lng
FROM rentDocument  
WHERE rentDocument.carSeq=(?) AND ( rentDocument.seq=(?) OR rentDocument.seq >= (?))
ORDER BY rentDocument.seq ${req.query.startAt ? 'ASC':'DESC'} /* if 'startAt' has been not input, that's mean is want to get last two docs  */
LIMIT 2
;`
        , [req.params.carSeq, req.query.startAt || 0, req.query.startAt || 0]
    )
        .then((carResult) => {
            if( !carResult[0] ){
                return res.status(404).json({
                    message:'not found'
                })
            }


            res.json(carResult.sort((a,b) => a.seq - b.seq));
        })
        .catch((e) => {
            console.error(e);
            res.status(500).json({ message: e.toString() });
        })
        .finally(() => {
            conn.release();

        })

})

module.exports = router;
