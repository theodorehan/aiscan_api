var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mariadb = require('mariadb');
var moment = require('moment')
var multer = require('multer')
var fs = require('fs-extra')
var upload = multer( )

var setup = require('./biz/startSetup');
var Worker = require('./biz/Worker');
var config = require('./config');
var indexRouter = require('./routes/index');
var clientRouter = require('./routes/client');
var captureRouter = require('./routes/capture');
var documentRouter = require('./routes/document');
var carRouter = require('./routes/car');

var app = express();
setup()


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/store',express.static(config.worker.storeDir));


console.log(process.execPath)

const pool = mariadb.createPool({
    host: config.db.host,
    user: 'aiscan',
    password:'aiscan',
    connectionLimit: 5,
    database: 'AISCAN'

});

const worker = new Worker(config.worker, pool);


const APIKeyFilter = async function(req,res,next){
    let key;
    if( req.method === 'POST' ){
        const contentType = req.headers['content-type'];
        if( contentType && contentType.indexOf('multipart/form-data;') === 0 ){

            next();
            return;
        } else {

            key = req.body.apiKey;
        }
    } else if ( req.method === 'GET' ){
        key = req.query.apiKey;
    }


    if( key === '3be91a7e-8c13-4bfc-ba5c-b30f57e068ae'){
        next();
    } else {
        res.status(401).send("Access Denied");
    }
};

app.use('/client', APIKeyFilter);
app.use('/capture', APIKeyFilter);

// prepare MariaDB interface
app.use(function(req,res,next){

  req.getConn = function(){
    return pool.getConnection()
  };
  req.getUTCTimestamp = function(){
      return moment().format('YYYY-MM-DD HH:mm:ss');
  }

  req.worker = worker;
  next();
})

app.use('/', indexRouter);
app.use('/client', clientRouter);
app.use('/capture', captureRouter);
app.use('/document', documentRouter);
app.use('/car', carRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
