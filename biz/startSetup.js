var fs = require('fs-extra')
const path = require('path');
var config = require('../config');
const locationOrder = [
    'front',
    'leftFront',
    'leftSide',
    'leftRear',
    'rear',
    'rightRear',
    'rightSide',
    'rightFront',
    'dashboard',
    'unknown'
]

module.exports = function(){
    ensureDirectory(config.worker.storeDir);
    ensureDirectory(config.worker.bundleDir);

    for(let i = 0; i < locationOrder.length; i++ ){
        ensureDirectory(path.join(config.worker.storeDir, locationOrder[i]));
    }
}

function ensureDirectory(dirPath) {
    try{
        const result = fs.statSync(dirPath);
        if( result.isDirectory() ){
            return;
        }
    } catch(e){
        console.info('No exists ' + dirPath + '. will make that directory');
        const result = fs.mkdirSync(dirPath);
    }

    const result = fs.statSync(dirPath);
    if( result.isDirectory() ){

        return;
    }
}