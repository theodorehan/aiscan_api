const fs = require('fs-extra');
const events = require('events');
const path = require('path');
const Promise = require('bluebird');
const moment = require('moment');
const Mutex = require('./Mutex');
const config = require('../config')
const TOTAL_PIC_COUNT = 71;
const axios = require('axios');
const FormData = require('form-data');
const ExifReader = require('jpeg-exif');
const locationOrder = [
    'front',
    'leftFront',
    'leftSide',
    'leftRear',
    'rear',
    'rightRear',
    'rightSide',
    'rightFront',
]
class Task {
    carSeq;
    DocId;
}

const getUTCTimestamp = function(){
    return moment().format('YYYY-MM-DD HH:mm:ss');
}

class Worker {
    config;
    maria = null;
    events = null;
    handlingTask = false;

    currentQueueItem = null;
    currentProgressStatus = null;

    constructor(workerConfig, mariaPool){
        this.config = workerConfig;
        this.maria = mariaPool;
        this.events = new events();
        this.mutex = new Mutex();
        // this.events.on('work', this.nextEventTicker);

        this.startTick();
    }

    startTick(){
        this.tickHandler();
    }


    async retrieveTask(){
        const conn = await this.maria.getConnection();

        const result = await conn.query(
                `
SELECT 
scanQueue.seq as taskId, 
car.carNo as carNo, 
rentDocument.seq as docId, 
rentDocument.action as action,
rentDocument.fileName as fileName,
scanQueue.progressStatus as progressStatus
FROM scanQueue 
LEFT JOIN (rentDocument) ON rentDocument.seq = scanQueue.rentDocumentSeq
LEFT JOIN (car) ON car.seq = rentDocument.carSeq
WHERE scanQueue.disabled=0 /* <> is not equal */
ORDER BY scanQueue.seq
LIMIT 1`
        );

        conn.release();

        return result[0];
    }

    async disableTask(task){
        const conn = await this.maria.getConnection();

        try{
            const result = await conn.query(
                `
UPDATE 
scanQueue
SET disabled=1 
WHERE seq=${task.taskId};`
            );
            console.log(result);
        } catch (e) {
            console.error('failed task disable', e);
        }


        conn.release();

    }

    async finishScan(task, latlng){
        const conn = await this.maria.getConnection();
        try{

            const result = await conn.query(
                "DELETE FROM scanQueue WHERE seq=(?)",
                [ task.taskId ]
            );
            console.log('finish scan task', result);

            if( latlng ){
                const update = await conn.query(
                    "UPDATE rentDocument SET lat=(?), lng=(?) WHERE seq=(?)",
                    [ latlng.lat || '', latlng.lng || '', task.docId ]
                );

                console.log('updated', update);
            }

        } catch (e) {
            console.log('error delete finished scan task', e)
        }
        conn.release();

    }

    async saveScanResult(damages = [], location, fileIndex, rentDocSeq, lat, lng){
        const conn = await this.maria.getConnection();


        const damageFlagVector = (new Array(11));

        for(let i =0; i < damageFlagVector.length; i++ ){
            damageFlagVector[i] = 0;
        }
        for( let i = 0; i < damages.length; i++ ){
            damageFlagVector[ damages[i] - 1 ] = 1;
        }


        try{
            const result = await conn.query(
                "INSERT INTO scanResult(rentDocumentSeq, finishDateTime, direction, fileIndex, lat, lng, damage1, damage2, damage3, damage4, damage5, damage6, damage7, damage8, damage9, damage10, damage11 ) " +
                `VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                [ rentDocSeq, getUTCTimestamp(), location, fileIndex,lat, lng, ...damageFlagVector ]
            );
        } catch (e) {
            console.error('save result error', e);
        }




        conn.release();
    }

    tickHandler = async () => {
        console.log('tick', new Date());
        await this.mutex.wait();
        await this.mutex.getLock();

        let task = await this.getTask();
        console.log('get task')
        if( task ){
            try{
                await this.handleTask(task);
            } catch (e) {
                console.error('failed to handle task', e)
                await this.disableTask(task);
            }
        } else {
            console.log('has no task to process')
        }



        process.nextTick(this.tickHandler);

        setTimeout(() => {
            this.mutex.unlock();
        }, 2000)
    }

    async getTask(){
        if( this ){


            return await this.retrieveTask();
        }
        return false;
    }

    async handleTask(task){


        // Main Promise
        return new Promise((resolve, reject) => {


            let mvaFilePath = path.join(this.config.bundleDir, 'rentdoc-'+ task.docId + '.mva');
            console.log('task',task)
            if(task.fileName){
                mvaFilePath = path.join(this.config.incomeRootDir, task.action.toUpperCase(), task.carNo, task.fileName);
                console.log('read file from ' + mvaFilePath);
            }

            // File Read Promise
            fs.readFile(mvaFilePath)
                .then(( file ) => {
                    let bufferLength = file.length;
                    let fileSizeList = [];

                    let cursor = 0;
                    while( cursor < bufferLength ){
                        let size = file.readUInt32LE(cursor);


                        fileSizeList.push({size, start:cursor+4});
                        cursor += 8 + size;
                    }

                    // File List processing Promise
                    Promise.mapSeries( fileSizeList, ( async (info, i) => {
                        if( i % 6 !== 0 ) return; // 6번마다 전송
                        const number = i / 6;
                        const buf = Buffer.alloc(info.size);
                        file.copy(buf, 0, info.start, info.start + info.size);



                        const form = new FormData();
                        form.append('image', buf, {filename: 'bar.jpg', contentType: 'image/jpeg' });


                        var lat = '', lng = '';
                        try{

                            // console.log('fileType', ExifReader.fromBuffer(buf));
                            const exif = ExifReader.fromBuffer(buf);
                            if( exif && exif.GPSInfo ){
                                lat = exif.GPSInfo.GPSLatitude.toString();
                                lng = exif.GPSInfo.GPSLongitude.toString();
                            }
                        }catch (e) {
                            console.log('exif read err', e)
                        }
                        try{
                            console.log('will send to ai', i, info.size);

                            const res = await new Promise((resolve, reject) => {
                                form.submit(config.ai.url, (err, res) => {

                                    if( err ){
                                        reject(err)
                                    } else {
                                        let data;
                                        res.on('data', (chunk => {
                                            const stringChunk = chunk.toString();
                                            console.log('ai chunk',stringChunk)
                                            try{

                                                data = JSON.parse(stringChunk);
                                            } catch (e){
                                                console.log('AI Response JSON Parsing Error', e)
                                            }
                                            // console.log('ccc', chunk.toString());
                                        }))

                                        res.on('end', () => {

                                            resolve(data);
                                        })
                                    }
                                })
                            })


                            const sideNumber = Math.floor(number / 7);
                            const location = locationOrder[sideNumber] || 'dashboard';

                            console.log('ai result', res)

                            if( res.success && res.predictions ){
                                if( res.predictions.length === 1 ){
                                    const prediction = res.predictions[0];

                                    await this.saveScanResult([prediction.damage], location, i, task.docId, lat, lng );
                                } else {
                                    console.log('detected more prediction', res.predictions);
                                }
                            } else {

                            }



                            await fs.writeFile(path.join(this.config.storeDir, location, `pic-${task.docId}-${i}.jpg`), buf)

                            return {lat,lng};
                        } catch (e) {
                            console.log('ai error', e)
                        }


                        // End of File List processing Promise
                    }))
                        .then( async (res) => {
                            console.log('complete', res)
                            const latlng = res.find((item) => {
                                if ( item ){
                                    if( item.lat && item.lng ){
                                        return true;
                                    }
                                }
                            })

                            await this.finishScan(task, latlng);


                            resolve();


                            // End of File List processing Promise Then
                        })
                        .catch(() => {
                            console.error('Error Worker::handleTask')


                            // End of File List processing Promise Catch
                        })

                    // End of File Read Promise
                })
                .catch((err) => {
                    console.error(err);


                    reject(err);
                    // End of File Read Promise Catch
                })



            //End Main Promise
        })
    }
}

module.exports = Worker;
