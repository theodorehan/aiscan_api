const events = require('events');


class Mutex{

    occupying = false;

    constructor(){
        this.event = new events();
    }


    wait(){
        return new Promise((resolve) => {
            if( this.occupying ){

                this.event.once('done', () => {
                    this.occupying = false;
                    resolve()
                });
                return;
            }

            resolve();
        })
    }

    getLock(){
        if( !this.occupying ){
            this.occupying = true;
        } else {

        }
    }

    unlock(){
        this.event.emit('done');
    }
}

module.exports = Mutex;
