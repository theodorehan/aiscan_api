const path = require('path')

module.exports = {
    db: {
      host: '192.168.0.99'
    },
    ai: {
      url: 'http://125.132.134.98:8888/predict'
    },
    worker : {
        incomeRootDir: `D:\\NEXM\\`,
        storeDir: 'D:\\NEXM\\scannedPictures\\',
        bundleDir: 'D:\\NEXM\\pictureBundles\\',
    }
}